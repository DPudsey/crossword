import React from "react";
import Crossword from "@jaredreisinger/react-crossword";
import "./TheCrossword.scss";

/////////////////////////////////////////////////
// I SEE YOU DEV TEAM.
// YOU GET OUTTA THE CODEBASE YOU CHEATING SCUMBAGS
// ESPECIALLY YOU MAX PORT.
/////////////////////////////////////////////////

const data = {
  across: {
    1: {
      clue:
        "Everything was nice. With my best friend the rabbit. Until they shot mum (5)",
      answer: "bambi",
      row: 0,
      col: 1
    },
    2: {
      clue:
        'Cheery old grandad. Doesn’t do health and safety. And "life finds a way" (8, 4)',
      answer: "jurassicpark",
      row: 2,
      col: 4
    },
    3: {
      clue:
        "A long time ago. and far, far, far, far away. My sister kissed me (4, 4)",
      answer: "starwars",
      row: 4,
      col: 0
    },
    4: {
      clue:
        'My kid has gone mad. "Your mother sucks cocks in hell!" Someone get a priest! (3, 8)',
      answer: "theexorcist",
      row: 6,
      col: 6
    },
    5: {
      clue:
        "People think I’m dumb. But I do law at Harvard. And the 'bend and snap' (7, 6)",
      answer: "legallyblonde",
      row: 8,
      col: 0
    },
    6: {
      clue:
        "Matchmaker helps out. Client does better that teacher. Oh the irony. (5)",
      answer: "hitch",
      row: 10,
      col: 11
    },
    7: {
      clue:
        'Luxurious ship. A love story doomed into the depths. "I\'ll never let you go..." (7)',
      answer: "titanic",
      row: 11,
      col: 4
    },
    8: {
      clue:
        "Lessons in this film: It's the inside that counts, and... Beastiality.\" (7)",
      answer: "beautyandthebeast",
      row: 17,
      col: 0
    }
  },
  down: {
    1: {
      clue:
        "My car is special. It lets me kiss my young mum. But she prefers dad (4, 2, 3, 6)",
      answer: "backtothefuture",
      row: 0,
      col: 1
    },
    2: {
      clue:
        "Neh neh, neh neh neh. Neh, neh neh, neh neh, neh neh. Neh neh, neh neh, neh... (4)",
      answer: "jaws",
      row: 2,
      col: 4
    },
    3: {
      clue:
        "A ring to rule them. And in the darkness bind them. Lost in a dark cave. (3, 6)",
      answer: "thehobbit",
      row: 4,
      col: 9
    },
    4: {
      clue:
        "Keiko was the star. When he jumped over the boy. And swam to freedom. (4, 5)",
      answer: "freewilly",
      row: 5,
      col: 12
    },
    5: {
      clue:
        '"I want to play a game". A bathtub and two hacksaws. Kill by 6PM. (3)',
      answer: "saw",
      row: 6,
      col: 15
    },
    6: {
      clue:
        "Almighty hammer. Can he be crowned king yet? Maybe save Earth first. (4)",
      answer: "thor",
      row: 11,
      col: 4
    },
    7: {
      clue:
        "Princess and pauper. And good sentient carpet. Make a whole new world. (7)",
      answer: "aladdin",
      row: 11,
      col: 7
    }
  }
};

const TheCrossword = () => {
  return (
    <Crossword
      data={data}
      highlightBackground="rgb(253, 219, 220)"
      columnBreakpoint="812px"
      gridBackground="rgb(24, 24, 24)"
      cellBorder="rgb(24, 24, 24)"
      textColor="rgb(24, 24, 24)"
      focusBackground="rgb(224, 239, 246)"
    />
  );
};

export default TheCrossword;
